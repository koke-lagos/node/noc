# Network Operation Center (NOC)

### Dependencias
  * Install `npm install cron`
  * Install `npm install dotenv | npm install env-var`
  * Install `npm install nodemailer`
  * Install `npm install mongoose`

### Loading Devs
  * Clonar el archivo .env.example a .env
  * Configurar las variables de entorno
  * Instalas modulos dependencias `npm install`
  * Levantar las bases de datos mongodb, postgres `docker compose up -d`
  * Ejecutar `npm run dev`

### ✅ Node con TypeScript y TS-Node-dev
  * Instalar TypeScript y demás dependencias
    ```
    npm install --save-dev typescript @types/node ts-node-dev rimraf
    ```
  * Inicializar el archivo de configuración de TypeScript
    ```
    npx tsc --init --outDir dist/ --rootDir src
    ```
  * Crear scripts para dev, build y start
    ```
    ➡️ noc\package.json

    "dev": "tsnd --respawn --clear src/app.ts",
    "build": "rimraf ./dist && tsc",
    "start": "npm run build && node dist/app.js"
    ```
  * Agregar configuración
    ```
    ➡️ noc\tsconfig.json

    "exclude": ["node_modules", "dist"],
    "include": ["src"],
    ```

### ✅ Prisma
  * Instalar `npm install prisma --save-dev`
  * Prisma ORM `npx prisma init --datasource-provider <PostgreSQL | SQLite | MongoDB | MySQL | MariaDB>`
  * Run a migration `npx prisma migrate dev --name init`