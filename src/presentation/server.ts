import { envs } from '../config/plugins/envs.plugin'
import { CheckService } from '../domain/use-cases/checks/check-service'
import { CheckServiceAll } from '../domain/use-cases/checks/check-service-all'
import { SendEmailLogs } from '../domain/use-cases/emails/send-email-logs'
import { FileSystemLogDatasource } from '../infrastructure/datasources/file-system-log.datasource'
import { MongoLogDatasource } from '../infrastructure/datasources/mongo-log.datasource'
import { PostgreLogDatasource } from '../infrastructure/datasources/postgre-log.datasource'
import { LogRepositoryImplements } from '../infrastructure/repositories/log.repository.implements'
import { CronService } from './cron/cron.service'
import { EmailService } from './email/email.service';

const fsLogRepository = new LogRepositoryImplements (new FileSystemLogDatasource)
const mongoLogRepository = new LogRepositoryImplements (new MongoLogDatasource)
const postgreLogRepository = new LogRepositoryImplements (new PostgreLogDatasource)

const emailService = new EmailService()

export class Server {

    public static async start() {
        console.log('Server started')

        // Enviar emails
        // new SendEmailLogs(emailService, fileSystemLogRepository).execute(
        //     ['jor.lagos.1988@gmail.com', 'jlagos@metrocapital.cl']
        // )

        // CronService.createJob('*/5 * * * * *', () => {
        //     const url = `https://google.com`
        //     // const url = `http://localhost:3000`
            
        //     new CheckService(
        //         logRepository,

        //         () => console.log(`${url} is ok`),
        //         // undefined,

        //         (error) => console.log(error),
        //         // undefined,
        //     ).execute(url)
        // })

        CronService.createJob('*/5 * * * * *', () => {
            const url = `https://google.com`
            // const url = `http://localhost:3000`
            
            new CheckServiceAll(
                [fsLogRepository, mongoLogRepository, postgreLogRepository],

                () => console.log(`${url} is ok`),
                // undefined,

                (error) => console.log(error),
                // undefined,
            ).execute(url)
        })
    }

}