import mongoose from 'mongoose'

export interface ConnectionOptions {
    mongoUrl: string
    dbName: string
}

export class MongoDatabase {

    static async connect(options: ConnectionOptions) {
        const { mongoUrl, dbName, } = options

        try {
            await mongoose.connect(mongoUrl, { dbName })

            console.log(`Mongoose successfully connected`)
        }
        
        catch (error) {
            console.log(`Mongoose connection error`)
            throw error
        }
    }

}