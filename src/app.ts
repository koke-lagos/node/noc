import { PrismaClient } from '@prisma/client'
import { envs } from './config/plugins/envs.plugin'
import { MongoDatabase } from './databases/mongo'
import { Server } from './presentation/server'

(async () => main())()

async function main() {

    await MongoDatabase.connect({ 
        mongoUrl: envs.MONGO_URL, 
        dbName: envs.MONGO_DB_NAME,
    })

    // const prisma = new PrismaClient()
    // const newLog = await prisma.logModel.create({
    //     data: {
    //         level: 'HIGH',
    //         message: 'Test message postgres and prisma',
    //         origin: 'app.ts'
    //     }
    // })

    // const logs = await prisma.logModel.findMany({
    //     where: {
    //         level: 'LOW'
    //     }
    // })

    Server.start()
}